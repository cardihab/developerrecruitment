# Senior Developer Programming Challenge.
Thanks for taking part in our challenge! Completing the challenge helps us understand your programming ability. It 
consists of two tasks: 1. Implement a REST API; 2. Write a SPA that calls your API after obtaining user input.

## Task 1
- Create a REST API service described at Service Requirements using your preferred language and any open source frameworks/libraries you 
require.
- Write unit tests to demonstrate that your API works.
    - Note: Full code coverage is not necessarily required.
- Submit
    - Source code, compiled file, build scripts, project files  and all accompanying libraries(via GitHub or other git repository)
    - Should be able to unzip files to a folder and run a start script/bat file to bring up api(with the instructions)
    - Bonus points: Make a docker image that starts your service.
## Task 2
- Create a SPA(Single page app) with any open source frameworks you like.
- Allow user to input the following:
    - mobile number
    - password
    - medication list
- Add a Submit button.
- Loop through medication list, for each medication call API you implemented in Task 1 and display the result on the page.
- Wait for 1 second between each call to Cardihab Search medicine API
- Your SPA should display any results it received from your API (if any) and inform the user of any errors.
- See [task2.mov](task2.mov)

# Service Requirements for Task 1
##### Service business logic
1. Call the Cardihab Login API to login once. The token will be expired in 24 hours. Quote this token as bearer auth token in the following call 
2. Call the Cardihab Search medicine API to get the medication search result list.

See: ![Task 1 business logic](task1.png)
##### Service Definition
```html
GET http://localhost:8080/search_medication
```
- Request Body
```json
{
	"username": "String",
	"password": "String",
	"medication_list": "String"
}
```
###### Success Response
- Response code
```html
200 OK
```
- Request Response
```json
[
    {
        "id": "900261000168107",
        "displayName": "Diacol tablet, 32",
        "appearance": "tablet",
        "doseType": "number"
    },
    {
        "id": "835751000168108",
        "displayName": "Dolaforte tablet, 20",
        "appearance": "tablet",
        "doseType": "number"
    }
]
```

##### Cardihab Login API Definition

```html
GET https://api-dev.cardihab.app/v1/login
```
- Request body:
```json
{
	"username": "0400000000",
	"password": "0400000000"
}
```
###### Success Response
- Response code
```html
200 OK
```
- Response body:
```
auth token
```
- Normal Response body is the auth token. This auth token will be quoted as the bearer token in the following API call
###### Unsuccessful Response
- Response code
```html
 403 Forbidden
```


##### Cardihab Search medicine API Definition
```html
GET https://api-dev.cardihab.app/v1/medications/search?count=100&term={term} 
```

- Where {term} is a string user provided (medication1 or medication2).
###### Success Response
- Response code
```html
200 OK
```

- Response Body
```json
[
    {
        "id": "900261000168107",
        "displayName": "Diacol tablet, 32",
        "appearance": "tablet",
        "doseType": "number"
    },
    {
        "id": "835751000168108",
        "displayName": "Dolaforte tablet, 20",
        "appearance": "tablet",
        "doseType": "number"
    }
]
```
###### Unsuccessful response
- Response code
```html
404 Not Found
```
- Response Body
```json
{
    "status": "NOT_FOUND",
    "timestamp": "2018-05-30T05:24:34.458",
    "message": "medication.not.found",
    "path": "/v1/medications/search",
    "details": [
        {
            "errorObject": "",
            "field": "",
            "errorValue": "tablet abc",
            "message": "entity.not.found"
        }
    ]
}
```