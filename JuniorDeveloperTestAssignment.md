# Senior Developer Programming Challenge.
Thanks for taking part in our challenge! Completing the challenge helps us understand your programming ability. It 
consists of one task: 1. Write a SPA that calls cardihab API after obtaining user input.

## Task 1
- Create a SPA(Single page app) with any open source frameworks you like.
- Allow user to input the following:
    - mobile number
    - password
    - medication list
- Add a Submit button that sends the input to the Cardihab API Call the Cardihab Login API to login once. The token will be expired in 24 hours. Quote this token as bearer auth token in the following call
- Loop through medication list, for each medication call Cardihab Search medicine API and display result on the page.
- Wait for 1 second between each call to Cardihab Search medicine API
- Your SPA should display any results it received from your API (if any) and inform the user of any errors.
- See [task.mov](task2.mov)

##### Cardihab Login API Definition

```html
GET https://api-dev.cardihab.app/v1/login
```
- Request body:
```json
{
	"username": "0400000000",
	"password": "0400000000"
}
```
###### Success Response
- Response code
```html
200 OK
```
- Response body:
```
auth token
```
- Normal Response body is the auth token. This auth token will be quoted as the bearer token in the following API call
###### Unsuccessful Response
- Response code
```html
 403 Forbidden
```


##### Cardihab Search medicine API Definition
```html
GET https://api-dev.cardihab.app/v1/medications/search?count=100&term={term} 
```

- Where {term} is a string user provided (medication1 or medication2).
###### Success Response
- Response code
```html
200 OK
```

- Response Body
```json
[
    {
        "id": "900261000168107",
        "displayName": "Diacol tablet, 32",
        "appearance": "tablet",
        "doseType": "number"
    },
    {
        "id": "835751000168108",
        "displayName": "Dolaforte tablet, 20",
        "appearance": "tablet",
        "doseType": "number"
    }
]
```
###### Unsuccessful response
- Response code
```html
404 Not Found
```
- Response Body
```json
{
    "status": "NOT_FOUND",
    "timestamp": "2018-05-30T05:24:34.458",
    "message": "medication.not.found",
    "path": "/v1/medications/search",
    "details": [
        {
            "errorObject": "",
            "field": "",
            "errorValue": "tablet abc",
            "message": "entity.not.found"
        }
    ]
}
```